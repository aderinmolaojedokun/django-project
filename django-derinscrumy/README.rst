================
Derin Scrumy
===============
derin scrumy is a simple Django app built by Derin
while learning about Django with Linuxjobber.
You can find detailed documentation in the "docs" directory.
Quick Start
-----------
1. Add "derinscrumy" to your INSTALLED_APPS in your settings.py file
like this:
	INSTALLED_APPS = [
                ...,
                'derinscrumy',
        ]
2. Include the oluwasayoscrumy URLConf in your project urls.py like this:
        path('derinscrumy/', include('derinscrumy.urls')),
3. Run 'python manage.py migrate' to create the derinscrumy models.
4. Start the development server,

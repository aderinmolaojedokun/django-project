from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.get_grading_parameters)
]

from django.shortcuts import render
from django.http import HttpResponse
from .models import GoalStatus, ScrumyGoals, ScrumyHistory
from django.contrib.auth.models import User
import random


# Create your views here.
def get_grading_parameters(request):
    context = ScrumyGoals.objects.filter(goal_name='Learn Django').first()
    try:
        context = context.goal_name
        return HttpResponse(context)
    except AttributeError:
        return HttpResponse("Goal name does not exist.")

def move_goal(request, goal_id):
	try:
		goal = ScrumyGoals.objects.get(goal_id=goal_id)
		context = "Goal name is %s"
		return HttpResponse(context % goal.goal_name)
	except ScrumyGoals.DoesNotExist:
		return render(request, 'goals/exception.html',
						{'error': 'A record with that goal does not exist'})

def add_goal(request):
    goal_id = random.randint(1000, 9999)
    list_of_id = list(ScrumyGoals.objects.values_list('goal_id', flat=True))
    while goal_id in list_of_id:
        goal_id = random.randint(1000, 9999)
    goal_status=GoalStatus.objects.get(status_name='Weekly Goal')
    # users = ["louis"]
    user= User.objects.get(username='louis')
    samples = ["Learn Django", "Keep Learning Django"]
    add_goal=ScrumyGoals(goal_name=random.choice(samples), goal_id=goal_id, created_by='louis', moved_by='louis', goal_status=goal_status, user=user)
    add_goal.save()
    return HttpResponse("OK")

def home(request):
    # user = User.objects.get(username='louis')
    # output=ScrumyGoals.objects.filter(goal_name='Learn Django', user=user)
    # result=[]
    
    # for item in output:
    #     result.append({'goal_id' : item.goal_id, 'goal_name': item.goal_name, 'user': user})
    # context={"data": result}
    # return render(request, 'goals/home.html', context)

    users = User.objects.all()
    result=[]
    for user in users:
        weekly_result = user.goals.filter(goal_status__status_name='Weekly Goal')
        daily_result = user.goals.filter(goal_status__status_name='Daily Goal')
        verify_result = user.goals.filter(goal_status__status_name='Verify Goal')
        done_result = user.goals.filter(goal_status__status_name='Done Goal')

        result.append({'user': user,'weekly_result': weekly_result, 'daily_result': daily_result, 'verify_result': verify_result, 'done_result': done_result})
    context = {'data': result}
    return render(request, 'goals/home.html', context)

from django.urls import path, include
from . import views

app_name = 'derinscrumy'

urlpatterns = [
    path('', views.get_grading_parameters),
    path('movegoal/<int:goal_id>/', views.move_goal, name="goal_name"),
    path('addgoal/', views.add_goal, name="add_goal"),
    path('home/', views.home, name="home"),
    path('accounts/', include('django.contrib.auth.urls')), 
]
